package tripPricer.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class UUIDException extends Exception {

    private Logger LOGGER = LoggerFactory.getLogger(UUIDException.class);
    public UUIDException(String attractionID) {
        super("UUID attractionID invalid  : " + attractionID);
        LOGGER.error("UUID attractionID invalid  : " + attractionID);
    }
}